#####
# Repository
#####

output "clone_https" {
  value = bitbucket_repository.this["0"].clone_https
}

output "clone_ssh" {
  value = bitbucket_repository.this["0"].clone_ssh
}

output "uuid" {
  value = bitbucket_repository.this["0"].uuid
}
