## 1.6.1

- maintenance: migrate module to Terraform registry
- chore: update pre-commit dependencies

## 1.6.0

- fix: do not skip TLS validations for hooks
- maintenance: updates the project to modern standards

## 1.5.0

- feat: remove moved statement

## 1.4.0

- feat: allows to add avatars, with `var.avatar_href`
- fix: fixes running condition with `var.group_permissions`

## 1.3.0

- feat: adds `branch_restriction_push_allowed_users` variable to be able to whitelist some users for pushing on protected branches

## 1.2.0

- refactor: adds `for_each` for repository, to ease future `external_repository_id` variable
- feat: now adding groups to repository through `bitbucket_repository_group_permission` with new variable `group_permissions`
- feat: adds `bitbucket_hook` with new variable `hooks`
- feat: adds new variables/options:
   - `pipelines_enabled`
   - `scm`
   - `slug`
   - `website`
   - `has_issues`
   - `fork_policy`
   - `inherit_default_merge_strategy`
   - `inherit_branching_model`
- chore: pins pre-commit dependencies

## 1.1.0

- chore: removes experiment
- chore: pins pre-commit dependencies
- maintenance: sets minimum terraform version to 1.3+

## 1.0.0

- feat: outputs repository uuid
- feat: adds `require_no_changes_requested` by default
- feat: adds `require_tasks_to_be_completed` restriction by default
- feat: adds new options for repository:
   - `language`
- feat: adds basic branching model management with two new variables:
   - `branching_model_production`
   - `branching_model_development`
- maintenance: migrate to `DrFaust92/bitbucket` provider
- chore: pins pre-commit dependencies

## 0.2.0

- maintenance: pins bitbucket provider to 2+
- chore: pins pre-commit dependencies

## 0.1.1

- fix: makes sure checks on `var.branch_protection_overrides` checks key, not value

## 0.1.0

- feat: adds is_private, has_wiki variables to control repository creation

## 0.0.0

- tech: initialise repository
