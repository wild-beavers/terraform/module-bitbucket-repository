locals {
  default_branch_name = "master"
  default_branch_protections = {
    require_approvals_to_merge : 1,
    push : null,
    require_passing_builds_to_merge : 1,
    delete : null
    require_default_reviewer_approvals_to_merge : 1,
    require_no_changes_requested : null,
    require_tasks_to_be_completed : null,
  }

  default_protection_branch_names = [local.default_branch_name]

  branch_protections_merge    = merge(local.default_branch_protections, var.branch_protection_overrides)
  branch_protections_key_list = setsubtract(keys(local.branch_protections_merge), var.branch_protection_ignore_rules)
  branch_protections_value_list = [
    for key in local.branch_protections_key_list : local.branch_protections_merge[key]
  ]
  branch_protections       = zipmap(local.branch_protections_key_list, local.branch_protections_value_list)
  branch_protections_names = compact(concat(local.default_protection_branch_names, var.protected_branches))

  all_branch_protections_rules_by_object = flatten([
    for rule, value in local.branch_protections : [
      for branch in local.branch_protections_names : {
        branch = branch
        rule   = rule
        value  = value
      }
    ]
  ])
  all_branch_protections_rules = {
    for element in local.all_branch_protections_rules_by_object :
    format("%s,%s", element.branch, element.rule) => element.value
  }
}

#####
# Repository
#####

locals {
  slug = var.slug != null ? var.slug : var.name
}

resource "bitbucket_repository" "this" {
  for_each = { 0 = "enabled" }

  owner       = var.owner
  name        = var.name
  slug        = local.slug
  description = var.description

  fork_policy                    = var.fork_policy
  has_issues                     = var.has_issues
  has_wiki                       = var.has_wiki
  inherit_branching_model        = var.inherit_branching_model
  inherit_default_merge_strategy = var.inherit_default_merge_strategy
  is_private                     = var.is_private
  language                       = var.language
  pipelines_enabled              = var.pipelines_enabled
  project_key                    = var.project_key
  scm                            = var.scm
  website                        = var.website

  dynamic "link" {
    for_each = var.avatar_href != null ? { 0 = var.avatar_href } : {}

    content {
      dynamic "avatar" {
        for_each = var.avatar_href != null ? { 0 = var.avatar_href } : {}

        content {
          href = avatar.value
        }
      }
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

#####
# Branching model
#####

locals {
  should_create_branching_model = var.branching_model_production != null && var.branching_model_development != null
}

resource "bitbucket_branching_model" "this" {
  for_each   = local.should_create_branching_model ? { 0 = 0 } : {}
  owner      = var.owner
  repository = bitbucket_repository.this["0"].name

  // This has idempotency issues with provider 2.20
  dynamic "development" {
    for_each = { 0 = var.branching_model_development }

    content {
      branch_does_not_exist = development.value.branch_does_not_exist
      use_mainbranch        = development.value.use_mainbranch
      name                  = development.value.name
    }
  }

  dynamic "production" {
    for_each = var.branching_model_production == null ? {} : { 0 = var.branching_model_production }

    content {
      enabled               = true
      branch_does_not_exist = production.value.branch_does_not_exist
      use_mainbranch        = production.value.use_mainbranch
      name                  = production.value.name
    }
  }
}

#####
# Branch protection
#####

resource "bitbucket_branch_restriction" "this" {
  for_each = local.all_branch_protections_rules

  owner      = var.owner
  repository = bitbucket_repository.this["0"].name

  kind    = split(",", each.key)[1]
  value   = each.value
  pattern = split(",", each.key)[0]
  users   = split(",", each.key)[1] == "push" ? var.branch_restriction_push_allowed_users : null
}

#####
# Reviewers
#####

resource "bitbucket_default_reviewers" "this" {
  owner      = var.owner
  repository = bitbucket_repository.this["0"].name

  reviewers = var.reviewers

  depends_on = [
    bitbucket_repository_group_permission.this
  ]
}

#####
# Group Permissions
#####

resource "bitbucket_repository_group_permission" "this" {
  for_each = var.group_permissions == null ? {} : var.group_permissions

  workspace  = var.owner
  repo_slug  = local.slug
  group_slug = each.key
  permission = each.value.permission

  depends_on = [
    bitbucket_repository.this
  ]
}

#####
# Webhooks
#####

resource "bitbucket_hook" "this" {
  for_each = var.hooks != null ? var.hooks : {}

  owner                  = var.owner
  repository             = bitbucket_repository.this["0"].name
  skip_cert_verification = false

  url         = each.value["url"]
  description = each.value["description"]
  events      = each.value["events"]
}
