# Terraform module Bitbucket repository

Handles bitbucket repositories with Terraform.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3 |
| bitbucket | ~> 2.30 |

## Providers

| Name | Version |
|------|---------|
| bitbucket | ~> 2.30 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [bitbucket_branch_restriction.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/branch_restriction) | resource |
| [bitbucket_branching_model.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/branching_model) | resource |
| [bitbucket_default_reviewers.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/default_reviewers) | resource |
| [bitbucket_hook.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/hook) | resource |
| [bitbucket_repository.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/repository) | resource |
| [bitbucket_repository_group_permission.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/repository_group_permission) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| avatar\_href | Href of the avatar for the repository | `string` | `null` | no |
| branch\_protection\_ignore\_rules | List of branch protection rules to ignore. Any rule in this list will be subtracted from default branch protection rules and var.branch\_protection\_overrides. | `list(string)` | `[]` | no |
| branch\_protection\_overrides | Additional branch protection rules to be added to the defaults. | `map(any)` | `{}` | no |
| branch\_restriction\_push\_allowed\_users | Users exempted of the branch restrictions for pushing. | `list(string)` | `null` | no |
| branching\_model\_development | The development branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a development property will leave the development branch unchanged.<br/>* name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).<br/>* use\_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.<br/>* branch\_does\_not\_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model. | <pre>object({<br/>    name                  = optional(string)<br/>    use_mainbranch        = optional(bool, true)<br/>    branch_does_not_exist = optional(bool)<br/>  })</pre> | `null` | no |
| branching\_model\_production | The production branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a production property will leave the production branch unchanged.<br/>* name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).<br/>* use\_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.<br/>* branch\_does\_not\_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model. | <pre>object({<br/>    name                  = optional(string)<br/>    use_mainbranch        = optional(bool)<br/>    branch_does_not_exist = optional(bool)<br/>  })</pre> | `null` | no |
| description | Description of the repository to create. | `string` | n/a | yes |
| fork\_policy | What the fork policy should be. Defaults to `allow_forks`. Valid values are `allow_forks`, `no_public_forks`, `no_forks`. | `string` | `null` | no |
| group\_permissions | Allows you set explicit group permission for a repository.<br/>Keys are Slug of the requested group.<br/><br/>  permission (string, optional) Permissions can be one of `read`, `write`, and `admin`. | <pre>map(object({<br/>    permission = optional(string, "read")<br/>  }))</pre> | `null` | no |
| has\_issues | If this should have issues turned on or not. | `bool` | `false` | no |
| has\_wiki | Whether or not the repository should have a wiki. | `bool` | `false` | no |
| hooks | Allows you to manage your webhooks on a repository.<br/>Key is free and might be anything<br/><br/>  url   (string, required) Where to POST to.<br/>  description  (string, required) Name / description to show in the UI.<br/>  events  (set(string), required) The events this webhook is subscribed to. Valid values can be found at [Bitbucket Event Payloads Docs](https://support.atlassian.com/bitbucket-cloud/docs/event-payloads/). | <pre>map(object({<br/>    url         = string<br/>    description = string<br/>    events      = set(string)<br/>  }))</pre> | `null` | no |
| inherit\_branching\_model | Whether to inherit branching model from project. | `bool` | `null` | no |
| inherit\_default\_merge\_strategy | Whether to inherit default merge strategy from project. | `bool` | `null` | no |
| is\_private | Whether or not the repository should be private or public. Careful, setting this to false means your repository will be at least readable for everybody in the world. | `bool` | `true` | no |
| language | What the language of this repository should be. | `string` | `null` | no |
| name | Name of the repository to create. | `string` | n/a | yes |
| owner | Owner (workspace) of the repository to create. | `string` | n/a | yes |
| pipelines\_enabled | Turn on to enable pipelines support. | `bool` | `null` | no |
| project\_key | Bitbucket project key where to create the repository. | `string` | n/a | yes |
| protected\_branches | Additional branch names to protect. This exclude default branch which will always be protected. | `list(string)` | `[]` | no |
| reviewers | List bitbucket user UUIDs of people to be added to the list of reviewers for the repository to create. | `list(string)` | `[]` | no |
| scm | What SCM you want to use. Valid options are `hg` or `git`. Defaults to `git`. | `bool` | `null` | no |
| slug | The slug of the repository. Defaults to `var.name`. | `string` | `null` | no |
| website | URL of website associated with this repository. | `bool` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| clone\_https | n/a |
| clone\_ssh | n/a |
| uuid | n/a |
<!-- END_TF_DOCS -->
